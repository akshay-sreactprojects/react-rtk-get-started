# react-rtk-get-started

This mini project demonstrates skills in:

- React
- Typescript
- Redux Toolkit
- RTK Query
- Gitlab CI
- managing sensitive credentials

In order to start this project you could either click [here](https://akshay-sreactprojects.gitlab.io/react-rtk-get-started/), or you could follow the following steps:

<ol> 
    <li> Clone this project.
    <li> Add your dogs api key in the <b><i>.env</i></b> file. Get the key [here](https://www.thedogapi.com/).
    <li> use <b><i>npm install</i></b> to install dependencies
    <li> use <b><i>npm run dev</i></b> to run the project in development.
</ol>
