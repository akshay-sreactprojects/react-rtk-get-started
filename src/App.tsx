import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { incremented, incrementedBy } from "./features/counter/counterSlice";
import { useFetchBreedsQuery } from "./features/dogs/dogsApiSlice";
import DenseAppBar from "./components/DenseAppBar";
import DogsRootComponent from "./components/DogsRootComponent";
import CountComponent from "./components/CountComponent";
import { Routes, Route, HashRouter } from "react-router-dom";
import IntroComponent from "./components/IntroComponent";
import { menuEntries } from "./utils/drawerEntries";

function App() {
  const route = useAppSelector((state) => state.router.path);
  return (
    <>
      <DenseAppBar />
      <div className="body">
        {menuEntries.map((menuEntry) =>
          menuEntry.path === route ? <menuEntry.component /> : null
        )}
      </div>
    </>
  );
}

export default App;
