import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { menuEntries } from "../utils/drawerEntries";
import { useAppDispatch } from "../app/hooks";
import { route } from "../features/router/routerSlice";

const DenseAppBar = () => {
  const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);
  const toggleDrawer = (open: boolean) => {
    setIsDrawerOpen(open);
  };
  const dispatch = useAppDispatch();

  const list = () => (
    <Box
      sx={{ width: 250 }}
      role="presentation"
      onClick={() => toggleDrawer(false)}
      onKeyDown={() => toggleDrawer(false)}
    >
      <List>
        {menuEntries.map((menuEntry) => (
          <ListItem
            key={menuEntry.name}
            disablePadding
            onClick={() => {
              dispatch(route(menuEntry.path));
            }}
          >
            <ListItemButton>
              <ListItemIcon>
                <menuEntry.icon />
              </ListItemIcon>
              <ListItemText primary={menuEntry.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar variant="dense">
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open menu"
              onClick={() => toggleDrawer(true)}
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" component="div">
              Samples
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>
      <Drawer
        anchor={"left"}
        open={isDrawerOpen}
        onClose={() => toggleDrawer(false)}
      >
        {list()}
      </Drawer>
    </>
  );
};

export default DenseAppBar;
