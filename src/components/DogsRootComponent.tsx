import { useState } from "react";
import { useFetchBreedsQuery } from "../features/dogs/dogsApiSlice";

const DogsRootComponent = () => {
  const [numDogs, setNumDogs] = useState(5);

  const { data = [], isFetching } = useFetchBreedsQuery(numDogs);

  return (
    <div>
      <div>
        <p>Dogs to fetch:</p>
        <select
          value={numDogs}
          onChange={(e) => setNumDogs(Number(e.target.value))}
        >
          <option value="5">5</option>
          <option value="10">10</option>
          <option value="15">15</option>
          <option value="20">20</option>
          <option value="200">200</option>
        </select>
      </div>

      <div>
        <p>Number of dogs fetched: {data.length}</p>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {data.map((breed) => (
              <tr key={breed.id}>
                <td>{breed.name}</td>
                <td>
                  <img src={breed.image.url} alt={breed.name} width={250}></img>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default DogsRootComponent;
