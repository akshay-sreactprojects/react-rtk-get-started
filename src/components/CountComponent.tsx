import { useAppDispatch, useAppSelector } from "../app/hooks";
import { incremented, incrementedBy } from "../features/counter/counterSlice";

const CountComponent = () => {
  const dispatch = useAppDispatch();
  const count = useAppSelector((state) => state.counter.value);
  const handleClick = () => dispatch(incremented());
  const handleClickNoChill = () => dispatch(incrementedBy(5));

  return (
    <>
      <p>
        <button type="button" onClick={handleClick}>
          Normal increment. count is: {count}
        </button>
      </p>
      <p>
        <button type="button" onClick={handleClickNoChill}>
          No chill increment. count is: {count}
        </button>
      </p>
    </>
  );
};

export default CountComponent;
