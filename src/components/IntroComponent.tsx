const IntroComponent = () => {
  return (
    <>
      <p>
        Please select a sample from the menu to explore my experiments with
        react. 🧔🏻
      </p>
    </>
  );
};

export default IntroComponent;
