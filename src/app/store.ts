import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "../features/counter/counterSlice";
import routerReducer from "../features/router/routerSlice";
import { dogsApiSlice } from "../features/dogs/dogsApiSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    router: routerReducer,
    [dogsApiSlice.reducerPath]: dogsApiSlice.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(dogsApiSlice.middleware);
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
