export const isValid = (value: string | undefined | null) =>
  value !== null && value !== undefined && value !== "";

export const castStringEnvVar = (value: string | undefined | null) =>
  value !== null && value !== undefined ? value : "";
