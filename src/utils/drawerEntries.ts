import HomeIcon from "@mui/icons-material/Home";
import PetsIcon from "@mui/icons-material/Pets";
import AddIcon from "@mui/icons-material/Add";
import { MenuEntry } from "../utils/types";
import IntroComponent from "../components/IntroComponent";
import CountComponent from "../components/CountComponent";
import DogsRootComponent from "../components/DogsRootComponent";

export const menuEntries: MenuEntry[] = [
  {
    name: "Start page",
    divider: false,
    path: "/",
    icon: HomeIcon,
    component: IntroComponent,
  },
  {
    name: "Redux Counter",
    divider: false,
    path: "/counter",
    icon: AddIcon,
    component: CountComponent,
  },
  {
    name: "RTK Query - Dogs API",
    divider: false,
    path: "/puppers",
    icon: PetsIcon,
    component: DogsRootComponent,
  },
];
