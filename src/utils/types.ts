export interface MenuEntry {
  name: string;
  path: string;
  icon: any;
  divider: boolean;
  component: () => JSX.Element;
}
