import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface CounterState {
  value: number;
}

const initialState: CounterState = {
  value: 0,
};

const counterSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    incremented(state) {
      state.value++;
    },
    incrementedBy(state, action: PayloadAction<number>) {
      state.value += action.payload;
    },
    decremented(state) {
      state.value--;
    },
  },
});

export const { incremented, incrementedBy, decremented } = counterSlice.actions;
export default counterSlice.reducer;
