import { BaseQueryFn } from "@reduxjs/toolkit/dist/query";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import axios, {
  AxiosBasicCredentials,
  AxiosError,
  AxiosRequestConfig,
} from "axios";
import { castStringEnvVar } from "../../utils/helpers";

const DOGS_API_KEY = castStringEnvVar(import.meta.env.REACT_APP_DOGS_API_KEY);

interface Breed {
  id: string;
  name: string;
  image: {
    url: string;
  };
}

const axiosBaseQuery =
  (
    { baseUrl }: { baseUrl: string } = { baseUrl: "" }
  ): BaseQueryFn<
    {
      url: string;
      method: AxiosRequestConfig["method"];
      auth?: AxiosBasicCredentials;
      headers?: AxiosRequestConfig["headers"];
      data?: AxiosRequestConfig["data"];
      params?: AxiosRequestConfig["params"];
    },
    unknown,
    unknown
  > =>
  async ({ url, method, auth, headers, data, params }, api) => {
    try {
      if (headers === undefined) {
        headers = {
          "x-api-key": DOGS_API_KEY,
        };
      } else {
        headers = {
          ...headers,
          "x-api-key": DOGS_API_KEY,
        };
      }

      const result = await axios({
        url: baseUrl + url,
        method,
        auth,
        data,
        params,
        headers,
      });
      return { data: result.data };
    } catch (axiosError) {
      const err = axiosError as AxiosError;
      return {
        error: {
          status: err.response?.status,
          data: err.response?.data || err.message,
        },
      };
    }
  };

export const dogsApiSlice = createApi({
  reducerPath: "dogsApi",
  baseQuery: axiosBaseQuery({
    baseUrl: "https://api.thedogapi.com/v1",
  }),
  endpoints: (builder) => {
    return {
      fetchBreeds: builder.query<Breed[], number | void>({
        query: (limit = 10) => ({
          url: `/breeds?limit=${limit}`,
          method: "get",
        }),
      }),
    };
  },
});

export const { useFetchBreedsQuery } = dogsApiSlice;
