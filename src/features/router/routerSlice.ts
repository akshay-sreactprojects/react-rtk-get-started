import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface RouterState {
  path: string;
}

const initialState: RouterState = {
  path: "/",
};

const routerSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    route(state, action: PayloadAction<string>) {
      state.path = action.payload;
    },
  },
});

export const { route } = routerSlice.actions;
export default routerSlice.reducer;
